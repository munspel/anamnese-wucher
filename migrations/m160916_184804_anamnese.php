<?php

use yii\db\Migration;
use yii\db\Schema;

class m160916_184804_anamnese extends Migration {

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
        $tableOptions = null;
        $this->createTable('{{%anamnese}}', [
            'anamneseid' => Schema::TYPE_PK,
            'userid' => Schema::TYPE_INTEGER,
            'notfallid' => Schema::TYPE_INTEGER,
            'bewertungid' => Schema::TYPE_INTEGER,
            'ersthelferbeurteilungid' => Schema::TYPE_INTEGER,
            'berufungsdiagnoseid' => Schema::TYPE_INTEGER,
            'notfall_zeit' => Schema::TYPE_TIMESTAMP,
            
            'is_medizinisch_indiziert' => Schema::TYPE_SMALLINT,
            'is_rettungstechnisch_indiziert' => Schema::TYPE_SMALLINT,
            
            'is_nkv' => Schema::TYPE_SMALLINT,
            'is_nka' => Schema::TYPE_SMALLINT,
            'is_nki' => Schema::TYPE_SMALLINT,
            
            'is_lenker' => Schema::TYPE_SMALLINT,
            'is_unbekannt' => Schema::TYPE_SMALLINT,
            'is_fusganger' => Schema::TYPE_SMALLINT,
            'is_mitfahrer' => Schema::TYPE_SMALLINT,
            
            'is_autobahn' => Schema::TYPE_SMALLINT,
            'is_prtsgebiet' => Schema::TYPE_SMALLINT,
            'is_uberland' => Schema::TYPE_SMALLINT,
            'is_seilbahn' => Schema::TYPE_SMALLINT,
            'is_schifffahrt' => Schema::TYPE_SMALLINT,
            'is_ortsgebiet' => Schema::TYPE_SMALLINT,
            'is_nicht_offent_strase' => Schema::TYPE_SMALLINT,
            
            'is_luftfahrzeug' => Schema::TYPE_SMALLINT,
            'is_moped' => Schema::TYPE_SMALLINT,
            'is_lkw' => Schema::TYPE_SMALLINT,
            'is_pkw' => Schema::TYPE_SMALLINT,
            'is_motorrad' => Schema::TYPE_SMALLINT,
            'is_autobus' => Schema::TYPE_SMALLINT,
            'is_radfahrer' => Schema::TYPE_SMALLINT,
            'is_zusammenstos' => Schema::TYPE_SMALLINT,
            'is_eisenbahn' => Schema::TYPE_SMALLINT,
            'is_landw' => Schema::TYPE_SMALLINT,
            'is_bfusganger' => Schema::TYPE_SMALLINT,
            'bisherige_krankheiten'=> Schema::TYPE_TEXT,
            'laufende_medikation'=> Schema::TYPE_TEXT,
            'notfallgeschehen'=> Schema::TYPE_TEXT,
            
            "is_seite_vollstandig"=>Schema::TYPE_SMALLINT,
            
                ], $tableOptions);
        $this->addForeignKey(
                "fk_notfallid",
                '{{%anamnese}}',
                'notfallid',
                '{{%cnotfall}}',
                'notfallid'
        ); 
        $this->addForeignKey(
                "fk_bewertungid",
                '{{%anamnese}}',
                'bewertungid',
                '{{%cbewertung}}',
                'bewertungid'
        ); 
        $this->addForeignKey(
                "fk_ersthelferbeurteilungid",
                '{{%anamnese}}',
                'ersthelferbeurteilungid',
                '{{%cersthelferbeurteilung}}',
                'ersthelferbeurteilungid'
        ); 
        $this->addForeignKey(
                "fk_berufungsdiagnoseid",
                '{{%anamnese}}',
                'berufungsdiagnoseid',
                '{{%cberufungsdiagnose}}',
                'berufungsdiagnoseid'
        ); 
    }

    public function safeDown() {
        $this->dropTable('{{%anamnese}}');
    }

}
