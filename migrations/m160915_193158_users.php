<?php
use yii\db\Schema;
use yii\db\Migration;

class m160915_193158_users extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
		 $tableOptions = null;
		 $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            //'role_id' => Schema::TYPE_INTEGER . ' not null',
            'status' => Schema::TYPE_SMALLINT . ' not null',
            'email' => Schema::TYPE_STRING . ' null',
            'username' => Schema::TYPE_STRING . ' null',
            'password' => Schema::TYPE_STRING . ' null',
            'auth_key' => Schema::TYPE_STRING . ' null',
            'access_token' => Schema::TYPE_STRING . ' null',
            'logged_in_ip' => Schema::TYPE_STRING . ' null',
            'logged_in_at' => Schema::TYPE_TIMESTAMP . ' null',
            'created_ip' => Schema::TYPE_STRING . ' null',
            'created_at' => Schema::TYPE_TIMESTAMP . ' null',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' null',
            'banned_at' => Schema::TYPE_TIMESTAMP . ' null',
            'banned_reason' => Schema::TYPE_STRING . ' null',
        ], $tableOptions);
		// insert admin user: neo/neo
		$security = Yii::$app->security;
		$columns = ['email', 'username', 'password', 'status', 'created_at', 'access_token', 'auth_key'];
        $this->batchInsert('{{%user}}', $columns, [
            [   
                'Stefan.ganahl@wucher.com',
                'Stefan.ganahl@wucher.com',
                '$2y$13$dyVw4WkZGkABf2UrGWrhHO4ZmVBv.K4puhOL59Y9jQhIdj63TlV.O', // neo
                1, // User::STATUS_ACTIVE
                gmdate('Y-m-d H:i:s'),
                $security->generateRandomString(),
                $security->generateRandomString(),
            ],
             [   
                'neo@neo.com',
                'admin',
                '$2y$13$dyVw4WkZGkABf2UrGWrhHO4ZmVBv.K4puhOL59Y9jQhIdj63TlV.O', // neo
                1, // User::STATUS_ACTIVE
                gmdate('Y-m-d H:i:s'),
                $security->generateRandomString(),
                $security->generateRandomString(),
            ],
            
        ]);

    }

    public function safeDown()
    {
		  $this->dropTable('{{%user}}');
    }
   
}
