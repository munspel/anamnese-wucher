<div class="row">
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-7 mb20">
                <div class="title">Notfall-kategorie</div>
                <div class="f-row bdrs dark">
                    <div class="control">
                        <select class="f-custom">
                            <option>Unfall Haushalt</option>
                            <option>Unfall Haushalt</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-5 mb20">
                <div class="title">Notfall-zeit</div>
                <div class="f-row bg-gr bdrs light">
                    <input class="form-control" type="time">
                </div>
                <div class="f-note">(Alarm  02:33)</div>
            </div>
        </div>
        <div class="title">Berufungsdiagnose/ Einsatzcode</div>
        <div class="f-row bg-gr bdrs light icon-r mb20">
            <div class="control">
                <select class="f-custom">
                    <option>01 C 2   Bauchschmerzen, -Be</option>
                    <option>01 C 2   Bauchschmerzen, -Be</option>
                </select>
            </div>
        </div>
        <div class="title">Bewertung</div>
        <div class="f-row bg-gr bdrs light mb20">
            <div class="control">
                <select class="f-custom">
                    <option>Zutreffend</option>
                    <option>Zutreffend</option>
                </select>
            </div>
        </div>
        <div class="titling bdrs mb20">
            <div class="caption">Der Einsatz war</div>
            <div class="wrap">
                <div class="row">
                    <div class="col-md-6">
                        <div class="f-row">
                            <input class="f-custom" type="checkbox" checked>Medizinisch indiziert
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="f-row">
                            <input class="f-custom" type="checkbox">Rettungstechnisch indiziert
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="title">Ersthelferbeurteilung</div>
        <div class="f-row bg-gr bdrs light mb20">
            <div class="control">
                <select class="f-custom">
                    <option>Keine</option>
                    <option>Keine</option>
                </select>
            </div>
        </div>
        <div class="titling bdrs mb20">
            <div class="caption">Notkompetenz(durch Ersthelfer)</div>
            <div class="wrap">
                <div class="row">
                    <div class="col-md-6">
                        <div class="f-row">
                            <input class="f-custom" type="checkbox" checked>NKV (Venenzugang)
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="f-row">
                            <input class="f-custom" type="checkbox">NKA (Arzneimittel)
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="f-row">
                            <input class="f-custom" type="checkbox">NKI (Intubation)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="title">Verkehrsunfall (VKU) Protokoll</div>
        <div class="f-row bg-gr bdrs light mb20">
            <div class="titling bdrs mb20">
                <div class="caption">Patient war</div>
                <div class="wrap">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox" checked>Lenker
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Fußgänger
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Mitfahrer
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Unbekannt
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="titling bdrs mb20">
                <div class="caption">Ort des Unfalls </div>
                <div class="wrap">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox" checked>Autobahn
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Überland
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Schifffahrt
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Ortsgebiet
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Seilbahn
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Nicht offent Straße
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="titling bdrs">
                <div class="caption">Beteiligte</div>
                <div class="wrap">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox" checked>Luftfahrzeug
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Motorrad
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Eisenbahn
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Moped
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Autobus
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Landw. FZ
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox" checked>LKW
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Radfahrer
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Fußgänger
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">PKW
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="f-row">
                                <input class="f-custom" type="checkbox">Zusammenstoß mehrerer gleichartig Beteiligter
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="title">Bisherige Krankheiten</div>
        <div class="f-row">
            <textarea class="form-control bg-gr">Österreich ist nicht nur ein Kulturstaat, sondern auch ein hochentwickeltes Industrieland. Sein Wirtschaft basiert auf einer reichen Auswahl von Bodenschätzen und großen Energiereserven. Die wichtigsten Industriezweige sind Maschinenbau und Fahrzeugbau, Textilindustrie, Nahrungsindustrie.</textarea>
        </div>
    </div>
</div>
<div class="title">Laufende Medikation</div>
<div class="f-row mb20">
    <textarea class="form-control bg-gr"></textarea>
</div>
<div class="title">Notfallgeschehen / Anamnese</div>
<div class="f-row mb20">
    <textarea class="form-control bg-gr">Österreich ist nicht nur ein Kulturstaat, sondern auch ein hochentwickeltes Industrieland. Sein Wirtschaft basiert auf einer reichen Auswahl von Bodenschätzen und großen Energiereserven. Die wichtigsten Industriezweige sind Maschinenbau und Fahrzeugbau, Textilindustrie, Nahrungsindustrie.</textarea>
</div>
<div class="footer-block text-center">
    <div><input class="f-custom" type="checkbox">Seite vollständig</div>
    <a href="#" class="btn">Speichern</a> <a href="#" class="btn btn-gr">Editieren</a>
</div>