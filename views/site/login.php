<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Wucher Login';
?>
<div class="login-form">
    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
    ]);
    ?>
    <?= $form->errorSummary($model); ?>
    <div class="f-row">
        <label>
            <span class="title">Benutzer</span>

            <?=
            Html::activeTextInput($model, 'username', [
                'autofocus' => true,
                    "type" => "email", 
                    "value" => "Stefan.ganahl@wucher.com"
            ]);
            ?>
        </label>
    </div>
    <div class="f-row">
        <label>
            <span class="title">Passwort</span>
            <?= Html::activePasswordInput($model, 'password', ["placeholder" => "********"]); ?>
        </label>
    </div>
    <div class="text-center">
        <button class="btn" type="submit">Anmelden</button>
        <div>
            <a href="#" class="forgot">Passwort vergessen?</a>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
