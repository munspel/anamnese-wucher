<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Anamnese */

$this->title = 'Update Anamnese: ' . $model->anamneseid;
$this->params['breadcrumbs'][] = ['label' => 'Anamnese', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->anamneseid, 'url' => ['view', 'id' => $model->anamneseid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="anamnese-update">

<!--    <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
