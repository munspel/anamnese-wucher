<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Anamnese';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anamnese-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Anamnese', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'anamneseid',
            'userid',
            'notfallid',
            'bewertungid',
            'ersthelferbeurteilungid',
            // 'berufungsdiagnoseid',
            // 'notfall_zeit',
            // 'is_medizinisch_indiziert',
            // 'is_rettungstechnisch_indiziert',
            // 'is_nkv',
            // 'is_nka',
            // 'is_nki',
            // 'is_lenker',
            // 'is_unbekannt',
            // 'is_fusganger',
            // 'is_mitfahrer',
            // 'is_Autobahn',
            // 'is_Ortsgebiet',
            // 'is_Überland',
            // 'is_Seilbahn',
            // 'is_nicht_offent_strase',
            // 'is_luftfahrzeug',
            // 'is_moped',
            // 'is_lkw',
            // 'is_pkw',
            // 'is_motorrad',
            // 'is_autobus',
            // 'is_radfahrer',
            // 'is_zusammenstos',
            // 'is_eisenbahn',
            // 'is_landw',
            // 'is_bfusganger',
            // 'bisherige_krankheiten:ntext',
            // 'laufende_medikation:ntext',
            // 'notfallgeschehen:ntext',
            // 'is_seite_vollstandig',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
