<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Anamnese */

$this->title = $model->anamneseid;
$this->params['breadcrumbs'][] = ['label' => 'Anamnese', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anamnese-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->anamneseid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->anamneseid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'anamneseid',
            'userid',
            'notfallid',
            'bewertungid',
            'ersthelferbeurteilungid',
            'berufungsdiagnoseid',
            'notfall_zeit',
            'is_medizinisch_indiziert',
            'is_rettungstechnisch_indiziert',
            'is_nkv',
            'is_nka',
            'is_nki',
            'is_lenker',
            'is_unbekannt',
            'is_fusganger',
            'is_mitfahrer',
            'is_autobahn',
            'is_ortsgebiet',
            'is_uberland',
            'is_seilbahn',
            'is_nicht_offent_strase',
            'is_luftfahrzeug',
            'is_moped',
            'is_lkw',
            'is_pkw',
            'is_motorrad',
            'is_autobus',
            'is_radfahrer',
            'is_zusammenstos',
            'is_eisenbahn',
            'is_landw',
            'is_bfusganger',
            'bisherige_krankheiten:ntext',
            'laufende_medikation:ntext',
            'notfallgeschehen:ntext',
            'is_seite_vollstandig',
        ],
    ]) ?>

</div>
