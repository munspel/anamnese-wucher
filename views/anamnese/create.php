<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Anamnese */

$this->title = 'Create Anamnese';
$this->params['breadcrumbs'][] = ['label' => 'Anamnese', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anamnese-create">

<!--    <h1><?= Html::encode($this->title) ?></h1>-->

    <?php echo $this->render('_form', ['model' => $model,]); ?>

</div>
