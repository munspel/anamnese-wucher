<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Anamnese */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="anamnese-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= Html::activeHiddenInput($model, 'userid', ['value' => Yii::$app->user->id]); ?>
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-7 mb20">
                    <div class="title">Notfall-kategorie</div>
                    <div class="f-row bdrs dark">
                        <div class="control">
                            <?= Html::activeDropDownList($model, "notfallid", ArrayHelper::map(\app\models\Cnotfall::find()->All(), "notfallid", "notfallname"), ["class" => "f-custom"]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 mb20">
                    <div class="title">Notfall-zeit</div>
                    <div class="f-row bg-gr bdrs light">
<!--                        <input class="form-control" type="time">-->
                        <?= Html::activeInput("text", $model, 'notfall_zeit', ['type' => 'time', 'class' => "form-control"]) ?>
                    </div>
                    <div class="f-note">(Alarm  02:33)</div>
                </div>
            </div>
            <div class="title">Berufungsdiagnose/ Einsatzcode</div>
            <div class="f-row bg-gr bdrs light icon-r mb20">
                <div class="control">
                    <?= Html::activeDropDownList($model, "berufungsdiagnoseid", ArrayHelper::map(\app\models\Cberufungsdiagnose::find()->All(), "berufungsdiagnoseid", "berufungsdiagnosename"), ["class" => "f-custom"]); ?>
                </div>
            </div>
            <div class="title">Bewertung</div>
            <div class="f-row bg-gr bdrs light mb20">
                <div class="control">
                    <?= Html::activeDropDownList($model, "bewertungid", ArrayHelper::map(\app\models\Cbewertung::find()->All(), "bewertungid", "bewertungname"), ["class" => "f-custom"]); ?>
                </div>
            </div>
            <div class="titling bdrs mb20">
                <div class="caption">Der Einsatz war</div>
                <div class="wrap">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="f-row">
                                <?= Html::activeCheckbox($model, "is_medizinisch_indiziert", ["class" => "f-custom", "label" => null]); ?> Medizinisch indiziert
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="f-row">
                                <?= Html::activeCheckbox($model, "is_rettungstechnisch_indiziert", ["class" => "f-custom", "label" => null]); ?>
                                Rettungstechnisch indiziert
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title">Ersthelferbeurteilung</div>
            <div class="f-row bg-gr bdrs light mb20">
                <div class="control">
                    <?= Html::activeDropDownList($model, "ersthelferbeurteilungid", ArrayHelper::map(\app\models\Cersthelferbeurteilung::find()->All(), "ersthelferbeurteilungid", "ersthelferbeurteilungname"), ["class" => "f-custom"]); ?>
                </div>
            </div>
            <div class="titling bdrs mb20">
                <div class="caption">Notkompetenz(durch Ersthelfer)</div>
                <div class="wrap">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="f-row">
                                <?= Html::activeCheckbox($model, "is_nkv", ["class" => "f-custom", "label" => null]); ?>NKV (Venenzugang)
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="f-row">
                                <?= Html::activeCheckbox($model, "is_nka", ["class" => "f-custom", "label" => null]); ?>NKA (Arzneimittel)
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="f-row">
                                <?= Html::activeCheckbox($model, "is_nki", ["class" => "f-custom", "label" => null]); ?>NKI (Intubation)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="vku-protocol">
                <div class="title">Verkehrsunfall (VKU) Protokoll</div>
                <div class="f-row bg-gr bdrs light mb20">
                    <div class="titling bdrs mb20">
                        <div class="caption">Patient war</div>
                        <div class="wrap">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_lenker", ["class" => "f-custom", "label" => null]); ?>Lenker
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_fusganger", ["class" => "f-custom", "label" => null]); ?>Fußgänger
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_mitfahrer", ["class" => "f-custom", "label" => null]); ?>Mitfahrer
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_unbekannt", ["class" => "f-custom", "label" => null]); ?>Unbekannt
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="titling bdrs mb20">
                        <div class="caption">Ort des Unfalls </div>
                        <div class="wrap">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_autobahn", ["class" => "f-custom", "label" => null]); ?>Autobahn
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_uberland", ["class" => "f-custom", "label" => null]); ?>Überland
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_schifffahrt", ["class" => "f-custom", "label" => null]); ?>Schifffahrt
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_ortsgebiet", ["class" => "f-custom", "label" => null]); ?>Ortsgebiet
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_seilbahn", ["class" => "f-custom", "label" => null]); ?>Seilbahn
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_nicht_offent_strase", ["class" => "f-custom", "label" => null]); ?>Nicht offent Straße
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="titling bdrs">
                        <div class="caption">Beteiligte</div>
                        <div class="wrap">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_luftfahrzeug", ["class" => "f-custom", "label" => null]); ?>Luftfahrzeug
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_motorrad", ["class" => "f-custom", "label" => null]); ?>Motorrad
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_eisenbahn", ["class" => "f-custom", "label" => null]); ?>Eisenbahn
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_moped", ["class" => "f-custom", "label" => null]); ?>Moped
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_autobus", ["class" => "f-custom", "label" => null]); ?>Autobus
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_landw", ["class" => "f-custom", "label" => null]); ?>Landw. FZ
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_lkw", ["class" => "f-custom", "label" => null]); ?>LKW
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_radfahrer", ["class" => "f-custom", "label" => null]); ?>Radfahrer
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_bfusganger", ["class" => "f-custom", "label" => null]); ?>Fußgänger
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_pkw", ["class" => "f-custom", "label" => null]); ?>PKW
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="f-row">
                                        <?= Html::activeCheckbox($model, "is_zusammenstos", ["class" => "f-custom", "label" => null]); ?>Zusammenstoß mehrerer gleichartig Beteiligter
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title">Bisherige Krankheiten</div>
            <div class="f-row">
                <?= Html::activeTextarea($model, "bisherige_krankheiten", ["class" => "form-control bg-gr"]); ?>
            </div>
        </div>
    </div>
    <div class="title">Laufende Medikation</div>
    <div class="f-row mb20">
        <?= Html::activeTextarea($model, "laufende_medikation", ["class" => "form-control bg-gr"]); ?>
    </div>
    <div class="title">Notfallgeschehen / Anamnese</div>
    <div class="f-row mb20">
        <?= Html::activeTextarea($model, "notfallgeschehen", ["class" => "form-control bg-gr"]); ?>
    </div>
    <div class="footer-block text-center">
        <div> <?= Html::activeCheckbox($model, "is_seite_vollstandig", ["class" => "f-custom", "label" => null]); ?>Seite vollständig</div>
        <!--        <a href="#" class="btn">Speichern</a> <a href="#" class="btn btn-gr">Editieren</a>-->
        <?= Html::submitButton($model->isNewRecord ? 'Speichern' : 'Editieren', ['class' => $model->isNewRecord ? 'btn' : 'btn btn-gr']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>


</div>
