<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Anamnese */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anamnese-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'anamneseid')->hiddenInput(); ?>
    <?= Html::activeHiddenInput($model, 'userid'); ?>

   
            <?= $form->field($model, 'notfallid')->textInput() ?>

            <?= $form->field($model, 'notfallid')->textInput() ?>

            <?= $form->field($model, 'bewertungid')->textInput() ?>

            <?= $form->field($model, 'ersthelferbeurteilungid')->textInput() ?>

            <?= $form->field($model, 'berufungsdiagnoseid')->textInput() ?>

            <?= $form->field($model, 'notfall_zeit')->textInput() ?>

            <?= $form->field($model, 'is_medizinisch_indiziert')->textInput() ?>

             <?= $form->field($model, 'is_rettungstechnisch_indiziert')->textInput() ?>

            <?= $form->field($model, 'is_nkv')->textInput() ?>

            <?= $form->field($model, 'is_nka')->textInput() ?>

            <?= $form->field($model, 'is_nki')->textInput() ?>

            <?= $form->field($model, 'is_lenker')->textInput() ?>

            <?= $form->field($model, 'is_unbekannt')->textInput() ?>

            <?= $form->field($model, 'is_fusganger')->textInput() ?>

            <?= $form->field($model, 'is_mitfahrer')->textInput() ?>

        <?= $form->field($model, 'is_autobahn')->textInput() ?>

        <?= $form->field($model, 'is_prtsgebiet')->textInput() ?>

        <?= $form->field($model, 'is_uberland')->textInput() ?>

        <?= $form->field($model, 'is_seilbahn')->textInput() ?>

        <?= $form->field($model, 'is_schifffahrt')->textInput() ?>

        <?= $form->field($model, 'is_nicht_offent_strase')->textInput() ?>
    
    
    

    <?= $form->field($model, 'is_luftfahrzeug')->textInput() ?>

    <?= $form->field($model, 'is_moped')->textInput() ?>

    <?= $form->field($model, 'is_lkw')->textInput() ?>

    <?= $form->field($model, 'is_pkw')->textInput() ?>

    <?= $form->field($model, 'is_motorrad')->textInput() ?>

    <?= $form->field($model, 'is_autobus')->textInput() ?>

    <?= $form->field($model, 'is_radfahrer')->textInput() ?>

    <?= $form->field($model, 'is_zusammenstos')->textInput() ?>

    <?= $form->field($model, 'is_eisenbahn')->textInput() ?>

    <?= $form->field($model, 'is_landw')->textInput() ?>

    <?= $form->field($model, 'is_bfusganger')->textInput() ?>
    
    

    <?= $form->field($model, 'bisherige_krankheiten')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'laufende_medikation')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'notfallgeschehen')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_seite_vollstandig')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
