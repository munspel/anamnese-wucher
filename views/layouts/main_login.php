<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\LoginAsset;

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login-tpl">
<?php $this->beginBody() ?>
    
    <div class="container-fluid">
        <div class="login-body">
            <div class="logo"><?= Html::img('@web/images/logo.png',['class'=>"img-responsive"]); ?></div>
            <div class="row">
                <div class="col-sm-4 col-lg-5 hidden-xs">
                    <div class="pic">
                        <?= Html::img('@web/images/icons/login.png',['class'=>"img-responsive"]); ?>
                    </div>
                </div>
                <div class="col-sm-8 col-lg-7">
                     <?= $content ?>
                </div>
            </div>
        </div>
    </div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
