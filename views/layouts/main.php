<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="btn-nav">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <nav class="nav-bar">
            <ul>
                <li class="active nav-item-0"><a href="#">Dashboard</a></li>
                <li class="nav-item-1"><a href="#">Dienstplan</a></li>
                <li class="nav-item-2"><a href="#">Flugeinsatz</a></li>
                <li class="nav-item-3"><a href="#">Mein dienstplan</a></li>
                <li class="nav-item-4"><a href="#">Einsätze</a></li>
                <li class="nav-item-5"><a href="#">Offene Einsätze</a></li>
                <li class="nav-item-6"><a href="#">Server</a></li>
                <li class="nav-item-7"><a href="#">Flugbuch</a></li>
                <li class="nav-item-8"><a href="#">Flight &amp; maintenance report</a></li>
                <li class="nav-item-9"><a href="#">Daily report</a></li>
                <li class="nav-item-10"><a href="#">Search F&amp;M Report</a></li>
                <li class="nav-item-11"><a href="#">Configuration list</a></li>
                <li class="nav-item-12"><a href="#">Open item list</a></li>
                <li class="nav-item-13"><a href="#">Fleet status export</a></li>
            </ul>
        </nav>
        <header class="top-block">
            <div class="logo">
                <a href="#"><?php echo Html::img('@web/images/wucher-logo.png'); ?></a>
            </div>
            <div class="wrap">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Hubshrauber/Flugvorbereitung</li>
                </ol>
                <div class="user-nav">
                    <div class="user-name-pic">
                        <div class="user-name">Hallo, <a data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout")?>">Stefan Ganahl!(Logout)</a></div>
                        <a href="#" class="user-pic"><i><?= Html::img('@web/images/temp/user-pic.png'); ?></i></a>
                    </div>
                </div>
            </div>
        </header>
        <section class="middle-block">
            <div class="container-fluid">
                <div class="wrapper case-history bg-w bdrs">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="heading">
                                <h1 class="mt0">Anamnese</h1>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <a href="#" class="back"><i class="fa fa-angle-left"></i> Zurück</a>
                        </div>
                    </div>
                    <div class="nav-inner">
                        <ul>
                            <li><a href="#">Einsatzlogistik</a></li>
                            <li><a href="#">Patientendaten</a></li>
                            <li class="onn"><a href="#">Anamnese</a></li>
                            <li><a href="#">Erstbefunde</a></li>
                            <li><a href="#">Erkrankungen<br>Verletzungen</a></li>
                            <li><a href="#">Maßnahmen</a></li>
                            <li><a href="#">Spez.Maßnahm.<br>Verlauf</a></li>
                            <li><a href="#">Medikamente<br>Material, Anm.</a></li>
                        </ul>
                    </div>
                    <div>
                        <?= $content; ?>
                    </div>

                </div>
            </div>
        </section>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
