var Anamnese =  Anamnese || {};
Anamnese.checkVKUProtocol = function (text) {
    if (text === "Unfall Arbeit") {
        $(".vku-protocol").show();

    } else {
        $(".vku-protocol").hide();
        $(".vku-protocol input").fancyfields("clean");
    }
}

$().ready(function () {

    $('.f-custom').fancyfields();


    $('.btn-nav').click(function () {
        $('body').toggleClass('openMenu');
    });

    
    Anamnese.checkVKUProtocol($( "#anamnese-notfallid option:selected" ).text());
    $("#anamnese-notfallid").fancyfields("bind", "onSelectChange", function (input, text, val) {
        Anamnese.checkVKUProtocol(text);
    });


});
