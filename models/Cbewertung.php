<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cbewertung".
 *
 * @property integer $bewertungid
 * @property string $bewertungname
 */
class Cbewertung extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cbewertung';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bewertungname'], 'required'],
            [['bewertungname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bewertungid' => 'Bewertungid',
            'bewertungname' => 'Bewertungname',
        ];
    }
}
