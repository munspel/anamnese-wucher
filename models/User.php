<?php

namespace app\models;
use app\models\DbUser as DbUser;
use Yii;

/**
* This is the model class for table "user".
*
* @property integer $id
* @property integer $status
* @property string $email
* @property string $username
* @property string $password
* @property string $auth_key
* @property string $access_token
* @property string $logged_in_ip
* @property string $logged_in_at
* @property string $created_ip
* @property string $created_at
* @property string $updated_at
* @property string $banned_at
* @property string $banned_reason
*/

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $auth_key;
    public $access_token;
	public $status;
	public $email;
	public $logged_in_ip;
	public $logged_in_at;
	public $created_ip;
	public $created_at;
	public $updated_at;
	public $banned_at;
	public $banned_reason;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id){
		$dbUser = DbUser::find()
				->where([
					"id" => $id
				])
				->one();
		if (!count($dbUser)) {
			return null;
		}
		return new static($dbUser);
    
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
       $dbUser = DbUser::find()
            ->where(["accessToken" => $token])
            ->one();
		if (!count($dbUser)) {
			return null;
		}
		return new static($dbUser);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $dbUser = DbUser::find()
            ->where([
                "username" => $username
            ])
            ->one();
		if (!count($dbUser)) {
			return null;
		}
		return new static($dbUser);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_key)
    {
        return $this->auth_key === $auth_key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
}
