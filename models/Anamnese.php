<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "anamnese".
 *
 * @property integer $anamneseid
 * @property integer $userid
 * @property integer $notfallid
 * @property integer $bewertungid
 * @property integer $ersthelferbeurteilungid
 * @property integer $berufungsdiagnoseid
 * @property string $notfall_zeit
 * @property integer $is_medizinisch_indiziert
 * @property integer $is_rettungstechnisch_indiziert
 * @property integer $is_nkv
 * @property integer $is_nka
 * @property integer $is_nki
 * @property integer $is_lenker
 * @property integer $is_unbekannt
 * @property integer $is_fusganger
 * @property integer $is_mitfahrer
 * @property integer $is_autobahn
 * @property integer $is_prtsgebiet
 * @property integer $is_uberland
 * @property integer $is_seilbahn
 * @property integer $is_schifffahrt
 * @property integer $is_nicht_offent_strase
 * @property integer $is_luftfahrzeug
 * @property integer $is_moped
 * @property integer $is_lkw
 * @property integer $is_pkw
 * @property integer $is_motorrad
 * @property integer $is_autobus
 * @property integer $is_radfahrer
 * @property integer $is_zusammenstos
 * @property integer $is_eisenbahn
 * @property integer $is_landw
 * @property integer $is_bfusganger
 * @property string $bisherige_krankheiten
 * @property string $laufende_medikation
 * @property string $notfallgeschehen
 * @property integer $is_seite_vollstandig
 */
class Anamnese extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'anamnese';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['userid', 'notfallid', 'bewertungid', 'ersthelferbeurteilungid', 'berufungsdiagnoseid', 'is_medizinisch_indiziert', 'is_rettungstechnisch_indiziert', 'is_nkv', 'is_nka', 'is_nki', 'is_lenker', 'is_unbekannt', 'is_fusganger', 'is_mitfahrer', 'is_autobahn', 'is_prtsgebiet', 'is_uberland', 'is_seilbahn', 'is_schifffahrt', 'is_nicht_offent_strase', 'is_luftfahrzeug', 'is_moped', 'is_lkw', 'is_pkw', 'is_motorrad', 'is_autobus', 'is_radfahrer', 'is_zusammenstos', 'is_eisenbahn', 'is_landw', 'is_ortsgebiet', 'is_bfusganger', 'is_seite_vollstandig'], 'integer'],
            [['notfall_zeit', "is_seite_vollstandig"], 'safe'],
            [['bisherige_krankheiten', 'laufende_medikation', 'notfallgeschehen'], 'string'],
            [['berufungsdiagnoseid'], 'exist', 'skipOnError' => true, 'targetClass' => Cberufungsdiagnose::className(), 'targetAttribute' => ['berufungsdiagnoseid' => 'berufungsdiagnoseid']],
            [['bewertungid'], 'exist', 'skipOnError' => true, 'targetClass' => Cbewertung::className(), 'targetAttribute' => ['bewertungid' => 'bewertungid']],
            [['ersthelferbeurteilungid'], 'exist', 'skipOnError' => true, 'targetClass' => Cersthelferbeurteilung::className(), 'targetAttribute' => ['ersthelferbeurteilungid' => 'ersthelferbeurteilungid']],
            [['notfallid'], 'exist', 'skipOnError' => true, 'targetClass' => Cnotfall::className(), 'targetAttribute' => ['notfallid' => 'notfallid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'anamneseid' => 'Anamneseid',
            'userid' => 'Userid',
            'notfallid' => 'Notfallid',
            'bewertungid' => 'Bewertungid',
            'ersthelferbeurteilungid' => 'Ersthelferbeurteilungid',
            'berufungsdiagnoseid' => 'Berufungsdiagnoseid',
            'notfall_zeit' => 'Notfall Zeit',
            'is_medizinisch_indiziert' => 'Is Medizinisch Indiziert',
            'is_rettungstechnisch_indiziert' => 'Is Rettungstechnisch Indiziert',
            'is_nkv' => 'Is Nkv',
            'is_nka' => 'Is Nka',
            'is_nki' => 'Is Nki',
            'is_lenker' => 'Is Lenker',
            'is_unbekannt' => 'Is Unbekannt',
            'is_fusganger' => 'Is Fusganger',
            'is_mitfahrer' => 'Is Mitfahrer',
            'is_autobahn' => 'Is Autobahn',
            'is_prtsgebiet' => 'Is Prtsgebiet',
            'is_uberland' => 'Is Uberland',
            "is_ortsgebiet" => "Is Ortsgebiet",
            'is_seilbahn' => 'Is Seilbahn',
            'is_schifffahrt' => 'Is Schifffahrt',
            'is_nicht_offent_strase' => 'Is Nicht Offent Strase',
            'is_luftfahrzeug' => 'Is Luftfahrzeug',
            'is_moped' => 'Is Moped',
            'is_lkw' => 'Is Lkw',
            'is_pkw' => 'Is Pkw',
            'is_motorrad' => 'Is Motorrad',
            'is_autobus' => 'Is Autobus',
            'is_radfahrer' => 'Is Radfahrer',
            'is_zusammenstos' => 'Is Zusammenstos',
            'is_eisenbahn' => 'Is Eisenbahn',
            'is_landw' => 'Is Landw',
            'is_bfusganger' => 'Is Bfusganger',
            'bisherige_krankheiten' => 'Bisherige Krankheiten',
            'laufende_medikation' => 'Laufende Medikation',
            'notfallgeschehen' => 'Notfallgeschehen',
            'is_seite_vollstandig' => 'Is Seite Vollstandig',
        ];
    }

    public function beforeSave($insert) {
        $t = explode(":", $this->notfall_zeit);
        if (count($t) > 1) {
            $this->notfall_zeit = Yii::$app->formatter->asDatetime(mktime($t[0], $t[1]), "php:Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public function afterFind() {
        $this->notfall_zeit = Yii::$app->formatter->asDatetime($this->notfall_zeit, "php:H:i");
        return parent::afterFind();
    }

}
