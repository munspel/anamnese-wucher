<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cersthelferbeurteilung".
 *
 * @property integer $ersthelferbeurteilungid
 * @property string $ersthelferbeurteilungname
 */
class Cersthelferbeurteilung extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cersthelferbeurteilung';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ersthelferbeurteilungname'], 'required'],
            [['ersthelferbeurteilungname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ersthelferbeurteilungid' => 'Ersthelferbeurteilungid',
            'ersthelferbeurteilungname' => 'Ersthelferbeurteilungname',
        ];
    }
}
