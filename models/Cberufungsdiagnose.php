<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cberufungsdiagnose".
 *
 * @property integer $berufungsdiagnoseid
 * @property string $berufungsdiagnosename
 */
class Cberufungsdiagnose extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cberufungsdiagnose';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['berufungsdiagnosename'], 'required'],
            [['berufungsdiagnosename'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'berufungsdiagnoseid' => 'Berufungsdiagnoseid',
            'berufungsdiagnosename' => 'Berufungsdiagnosename',
        ];
    }
}
