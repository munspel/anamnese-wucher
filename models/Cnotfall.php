<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cnotfall".
 *
 * @property integer $notfallid
 * @property string $notfallname
 */
class Cnotfall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cnotfall';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notfallname'], 'required'],
            [['notfallname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notfallid' => 'Notfallid',
            'notfallname' => 'Notfallname',
        ];
    }
}
